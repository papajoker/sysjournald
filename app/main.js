const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const fs = require('fs')
const consts = require('../app/consts.js')
const dico = consts.loadDico()
const UserConfig = new require('./userconfig.js')
const userConfig = new UserConfig()
const Timer = require('timers')
const timeout = 1000 * 60 * 30 // 30 minutes and close

app.set('view engine', 'ejs')

/* ------------ CONFIG --------------- */

app.use(express.static('assets')) // static
.use(bodyParser.urlencoded({ extended: true })) // parsing application/x-www-form-urlencoded

app.timeOutDate = new Date().valueOf() + timeout
app.use((req, res, next) => {
    app.timeOutDate = new Date().valueOf() + timeout // reset timeout
    next()
})

Timer.setInterval(()=>{
    if (new Date().valueOf() > app.timeOutDate) {
        console.log('-- inactivity close, last acces:', new Date(app.timeOutDate).toISOString())
        process.exit()
    }
}, Math.round(timeout / 10))

/* ------------ ROUTES --------------- */

app.get('/', (request, response) => {
    console.log(request.body)
    // response.json(request.body)
    response.render('index', {'dico': dico})
})

app.post('/exit', (request, response) => {
    response.end()
    console.log('-- app close')
    userConfig.save(()=>{
        setTimeout(() => { process.exit(0) }, 6000)
    })
})

/* ------------ ROUTE APP AJAX --------------- */

app.post('/api/logs', (request, response) => {
    console.log(request.body)
    let Logs = require('../models/logs')
    Logs.run(request.body, (datas) =>{
        datas.txt = Logs.toHtml(datas)
        response.send(datas)
    })
})

app.post('/api/bootlist', (request, response) => {
    console.log(request.body)
    let Boots = require('../models/bootlist')
    Boots.run(request.body, (datas) =>{
        // datas.txt = Logs.toHtml(datas)
        response.send(datas)
    })
})

// app.get('/api/unit/:id', (request, response) => {
app.post('/api/unit', (request, response) => {
    let Unit = require('../models/unit')
    Unit.run(request.body.unit, (content) =>{
        content = Unit.toHtml(content)
        response.render('unit', {
            title: request.body.unit,
            txt: content.txt,
            caption: dico.app.details,
            detail: content.detail
        })
    })
})

app.post('/api/pacman', (request, response) => {
    let Pacman = require('../models/pacman')
    Pacman.run(request.body, (content) =>{
        content = Pacman.toHtml(content)
        response.render('pacman', {
            'response': content
        })
    })
})

app.post('/api/man', (request, response) => {
    let Man = require('../models/man')
    Man.run(request.body.man, (content) =>{
        response.send(Man.toHtml(content))
    })
})

app.post('/api/mansearch', (request, response) => {
    let Man = require('../models/mansearch')
    Man.run(request.body.search, (content) =>{
        content = Man.toHtml(content)
        response.render('mansearch', {'response': content})
    })
})

app.post('/api/units', (request, response) => {
    let Units = require('../models/units')
    Units.run('', (content) =>{
        response.render('units', {
            units: content.units,
            displayManager: content.displayManager
        })
    })
})

app.post('/api/partitions', (request, response) => {
    let Partitions = require('../models/partitions')
    Partitions.run('', (content) =>{
        response.send(Partitions.toHtml(content))
    })
})

app.post('/api/exelist', (request, response) => {
    let ExeList = require('../models/exelist')
    ExeList.run('', (content) =>{
        content = ExeList.toHtml(content)
        response.render('exelist', {
            'lists': content,
            'dico': dico.html.type
        })
    })
})

app.get('/boot', (request, response) => {
    let Boot = require('../models/boot')
    Boot.run('', (src) =>{
        response.render('boot-graph', {'svg': src})
    })
})

app.route('/editor')
    .get(function (req, res) {
        res.send('Get a random book')
    })
    .post(function (req, res) {
        res.send('Add a book')
    })
    .put(function (req, res) {
        res.send('Update the book')
    })


/* ------------ RUN --------------- */

/*
* only one instance ...
*/

function checkIfNotIsRunning (cb) {
    let net = require('net')
    let server = net.createServer(function (c) { })
    server.listen('8082', () => {
        server.close()
        console.log('-- not used')
        cb()
    })
    server.on('error', (e) => {
        if (e.code == 'EADDRINUSE') {
            console.log('-- used')
        }
    })
}

checkIfNotIsRunning (() => {
    fs.existsSync(consts.tmp) || fs.mkdir(consts.tmp)
    userConfig.load()
    app.listen(8082)
})

require('open')('http://localhost:8082')
