'use strict'

// require('./dialog')

/* ----------- SEARCH ACTION -------------- */

/*
 *   send message get logs to (server)
 *   inputs : main form
 */
document.getElementById('send-btn').addEventListener('click', () => {
    event.preventDefault()
    $.ajax({
        method: "POST",
        url: "api/logs",
        data: {
            'search': document.getElementById('search').value,
            'type': document.getElementById('searchtype').value,
            'boot': document.getElementById('boot').value,
            'date': document.getElementById('date').value,
            'level': document.getElementById('level').value
        }
    })
    .done((response) => {
        $('#content').prepend(`<pre>` + /* ${response.txt}+*/ `</pre><h4>${response.error}</h4>`)
        $('#commandbash').html(response.bash)
        $('#logs').append(response.txt)

        /* dialog message detail */
        document.querySelectorAll('span.time').forEach((el) => {
            el.addEventListener('click', (event) => {
                let logtime = event.srcElement.textContent
                let response = event.srcElement.parentNode.querySelector('.cache').textContent
                let item = JSON.parse(response)
                response = `<p bg-info><div class="well well-sm"><b>${item['MESSAGE']}</b></div></p><hr />`
                for (let key in item) {
                    (key != 'MESSAGE') && (response += `<span class="text-info">${key}</span> = ${item[key]}<br />`)
                }
                $('#dialog .modal-title').html(`<h5 class="text-muted">${logtime}</h5>`)
                document.querySelector('#dialog .modal-body').innerHTML = response
                $('#dialog').modal('show')
            })
        })

        /* dialog unit detail */
        document.querySelectorAll('span.unit').forEach((el) => {
            el.addEventListener('click', (event) => {
                let unit = event.srcElement.getAttribute('data-unit')
                $.ajax({
                    method: "POST",
                    url: "/api/unit",
                    data: {'unit': unit}
                })
                .done((response) => {
                    dialog.title = unit
                    dialog.body = response
                    dialog.show()
                })
            }, false)
        })

        /* dialog package detail */
        document.querySelectorAll('span.executable').forEach((el) => {
            el.addEventListener('click', (event) => {
                $.ajax({
                    method: "POST",
                    url: "/api/pacman",
                    data: {
                        lang: navigator.language.slice(0, 2),
                        exe: event.srcElement.textContent
                    }
                })
                .done((response) => {
                    dialog.title = event.srcElement.textContent,
                    dialog.body = response
                    dialog.show()
                })
            }, false)
        })

    })
})

/* --------------- HTML ---------------------- */


function getBootList () {
    $.ajax({
        method: "POST",
        url: "/api/bootlist"
    })
    .done((response) => {
        let selected = 'selected'
        response.items.forEach((item) => {
            $('#boot').append(`<option ${selected} value="${item.id}">${item.date} &nbsp;&nbsp; ${item.time.slice(0, -3)} &nbsp;&nbsp; ${item.day}</option>`)
            selected = ''
        })
    })
}

function getExeList () {
    $.ajax({
        method: "POST",
        url: "/api/exelist"
    })
    .done((response) => {
        dialog.title = 'search input',
        dialog.body = response
        dialog.show()
    })
}

window.addEventListener('load', () => {
    dialog.init()
    getBootList()



    document.getElementById('search-exe').addEventListener('click', (event) => {
        event.preventDefault()
        getExeList()
        // ipcRenderer.send('consts.events.JOURNAL_GET_EXES')
    }, false)

    document.getElementById('search').addEventListener('blur', () => {
        if (document.getElementById('search').value.slice(0, 1) == '/') {
            document.getElementById('searchtype').selectedIndex = 1
        }
    }, false)

    $(document).on('click', 'a[target^="edit"]', (event) => {
        // open self editor
        event.preventDefault()
        let href = event.target.href
        if (href.slice(0, 7) == 'file://') href = href.slice(7)
    })

    document.getElementById('menu1').addEventListener('click', (event) => {
        event.preventDefault()
        $.ajax({
            method: "POST",
            url: "exit"
        })
        setTimeout(() => { window.close() }, 600)
    }, false)

    document.getElementById('menu2').addEventListener('click', (event) => {
        event.preventDefault()
        $.ajax({
            method: "POST",
            url: "/api/units"
        })
        .done((response) => {
            // dialog.title = dico.menu.units,
            dialog.body = response
            dialog.show()
        })
    }, false)

    document.getElementById('menu3').addEventListener('click', (event) => {
        event.preventDefault()
        window.open('/boot', 'web')
    }, false)

    document.getElementById('menu4').addEventListener('click', (event) => {
        event.preventDefault()
        $.ajax({
            method: "POST",
            url: "/api/partitions"
        })
        .done((response) => {
            dialog.title = 'df -h / lsblk',
            dialog.body = response
            dialog.show()
        })
    }, false)

    document.getElementById('menu5').addEventListener('click', (event) => {
        event.preventDefault()
        showDialogMan('')
    }, false)

    document.getElementById('menu6').addEventListener('click', (event) => {
        event.preventDefault()
        // max item, .... et about
    }, false)


})

