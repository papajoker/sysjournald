/*
*   run a pacman Qi
*/

const exec = require('child_process').exec
// const execSync = require('child_process').execSync
// const consts = require('../app/consts.js')

class Pacman {

    static run (request, cb) {
        // console.log('module pacman', request.exe)
        if (request.exe) {
            exec(`pacman -Qoq ${request.exe}`, (err, stdout) => {
                if (!err) {
                    request.unit = stdout.slice(0, -1)
                    this.runQi(request, cb)
                }
            })
        }
        else {
            this.runQi(request, cb)
        }
    }

    // run a pacman Qi
    static runQi (request, cb) {
        // console.log('module pacman Qi')
        let response = {
            caption: '',
            unit: request.unit,
            qi: '',
            links: [],
            ql: '',
            mans: '',
            err: ''
        }

        request.lang = request.lang + '_' + request.lang.toUpperCase() + '.utf8'
        console.log(`LANG=${request.lang} pacman -Qi "${response.unit}";`)
        exec(`pacman -Qi "${response.unit}"| grep -v "[--]$" `, {
            env: {
                TERM: 'xterm',
                LANG: request.lang
            }
        },
            (err, stdout, stderr) => {
                if (!err) {
                    response.qi = stdout
                    console.log(`echo -n $(LANG=C pacman -Qi ${response.unit} | awk -F: '/^Requi|Depend/{print $2}')`)
                    exec(`echo -n $(LANG=C pacman -Qi ${response.unit} | awk -F: '/^Requi|Depend/{print $2}')`, (err, stdout) => {
                        response.links = stdout.split(' ')
                        exec(`pacman -Ql "${response.unit}"| grep -v "\/$"|awk '{print $2}'`,
                            (err, stdout, stderr) => {
                                if (!err) {
                                    response.ql = stdout

                                    let find = request.unit.match(/\w+$/)[0]
                                    console.log('man -k', find)
                                    exec(`man -k "${find}"`, (err, stdout) => {
                                        if (!err) response.mans = stdout
                                        if (cb) cb(response)
                                    })
                                } else {
                                    response.err = stderr
                                    console.log('bash ERROR', stderr)
                                }
                            })
                    })
                } else {
                    console.log('bash ERROR', stderr)
                }
            })
        return false
    }

    static toHtml (response) {
        response.qi = response.qi.replace(/^(\w.*?):/gm, '<span class="text-info">$1</span>=')

        function setLink (link) {
            let ret = (link.charAt(link.length - 1) == "\n") ? "\n" : ''
            let linkc = link.trim()
            link = linkc
            let plus = ''
            let pos = link.indexOf('>')
            if (pos > -1) {
                link = link.slice(0, pos)
                plus = linkc.slice(pos)
            }
            return ` <a href="#pacman/ql/${link}" class="getQI">${link}</a>${plus}&nbsp;&nbsp; ${ret}`
        }

        response.links.forEach((value) => {
            response.qi = response.qi.replace(new RegExp(` (${value})[ |\n]`, 'g'), setLink)
        })
        response.qi = response.qi.replace(
            new RegExp('(https?://[-a-z0-9:%_\+.~#;?&//=]{4,})', 'gmi'),
            `<a href="$1" title="$1" class="icn icn-external-link" target="web">&nbsp;$1</a>`
        )
        response.qi = response.qi.replace(/\n/g, '<br />')

        response.ql = response.ql.replace(/^(\/usr\/bin\/\w.*)/gm, '<span class="text-info">$1</span>')
        response.ql = response.ql.replace(/^(\/usr\/share\/doc.*index\.html$)/gm, '<span class="text-info"><a href="file://$1" target="web" class="icn-external-link">$1</a></span>')
        response.ql = response.ql.replace(/^(\/etc\/.*\.conf$)/gm, '<a target="edit" href="file://$1">$1</a>')
        response.ql = response.ql.replace(/^(\/etc\/\w.*)/gm, '<span class="text-info">$1</span>')
        response.ql = response.ql.replace(/(\.desktop)$/gm, '<span class="text-info">$1</span>')
        response.ql = response.ql.replace(/\n/g, '<br />')

        response.mans = response.mans.replace(/^(.*)\([\d|\)]/gm, '<a class="icn icn-info-circle man" aria-hidden="true" href="#"> $1</a> (')
        response.mans = response.mans.replace(/\n/g, '<br />')

        return response
    }

}

module.exports = Pacman
