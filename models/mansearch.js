/*
*   search in man
*/

const exec = require('child_process').exec
// const consts = require('../app/consts.js')

class ManSearch {
    static run (search, cb) {
        console.log(`man -k "${search}"`)
        let response = {txt: ' '}
        response.caption = ''
        if (search) {
            exec(`man -k "${search}"`, (err, stdout, stderr) => {
                response.txt = stdout
                response.caption = search
                if (stderr) response.txt = stderr
                cb(response)
            })
        }
        else {
            cb(response)
        }
    }

    static toHtml (response) {
        response.txt = response.txt.replace(/^(.*)\([\d|\)]/gm, '<a class="icn icn-info-circle man" aria-hidden="true" href="#"> $1</a> (')
        response.txt = response.txt.replace(/\n/g, '<br />')
        return response
    }
}

module.exports = ManSearch
