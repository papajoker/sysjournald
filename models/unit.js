/*
*   get init systemd info
*/

const exec = require('child_process').exec
// const consts = require('../app/consts.js')

class Unit {

    static run (unitName, cb) {
        console.log('module unit')
        let response = {}
        exec(`systemctl cat ${unitName}`, {
            env: {
                TERM: 'xterm' // no colors
            }
        },
        (err, stdout, stderr) => {
            if (!err) {
                // response.caption = unitName
                response.txt = stdout
                exec(`systemctl show ${unitName} --no-pager`, (err, stdout, stderr) => {
                    if (!err) {
                        response.detail = stdout
                        cb(response)
                        return true
                    } else {
                        console.log('bash ERROR', stderr)
                    }
                })
            } else {
                console.log('bash ERROR', stderr)
            }
        })
        return false
    }

    static toHtml (content) {
        content.txt = content.txt.replace(/^#(.*)\n/gm, '<i class="text-muted">#$1</i>' + '\n')
        content.txt = content.txt.replace(/^\[(.*)\]$/gm, '[<em class="text-primary">$1</em>]')
        content.txt = content.txt.replace(/^(\w.*?)=/gm, '<span class="text-info">$1</span>=')
        content.txt = content.txt.replace(/\n/gm, '<br />')

        content.detail = content.detail.replace(/^(\w.*?)=/gm, '<span class="text-info">$1</span>=')
        content.detail = content.detail.replace(/\n/gm, '<br />')

        return content
    }

}

module.exports = Unit
