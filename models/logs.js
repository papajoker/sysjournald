const exec = require('child_process').exec
const consts = require('../app/consts.js')
const dico = consts.loadDico()

class MessageItem {
    constructor (item, dico) {
        this.items = item
        this.dico = dico
        this.items.date = new Date(item.__REALTIME_TIMESTAMP / 1000).toLocaleString()
        this.items.level = this.dico.html.levels[this.items.PRIORITY]
    }

    get isUnit () {
        return (this.items._SYSTEMD_UNIT != undefined)
    }
    get isExe () {
        return (this.items._EXE != undefined)
    }
    get _SYSTEMD_UNIT () {
        return (this.isUnit) ? `<span class="unit label label-default" title="${this.dico.logs.unit}" data-unit="${this.items._SYSTEMD_UNIT}">${this.items._SYSTEMD_UNIT}</span>` : '<span></span>'
    }
    get _UID () {
        return (this.items._UID != undefined) ? `<span class="uid badge" title="UID" >${this.items._UID}</span>` : ''
    }
    get level () {
        return this.dico.html.levels[this.items.PRIORITY]
    }
    get EXE () {
        return `<span class="exe label label-info${(this.isExe) ? ' executable' : ''}">${(this.items._EXE == undefined) ? ((this.items._COMM == undefined) ? this.items.SYSLOG_IDENTIFIER : this.items._COMM) : this.items._EXE}</span>`
    }
    toJson () {
        return JSON.stringify(this.items)
    }

    render () {
        return `<li class="priority${this.items.PRIORITY}">
                <span class="time label label-default" title="${this.dico.logs.detail}">${this.items.date}</span>
                ${this.EXE}
                <span class="priority badge" title="${this.items.level}">${this.items.PRIORITY}</span>
                <em class="msg">${this.items.MESSAGE}</em>
                ${this._SYSTEMD_UNIT}
                ${this._UID}
                <div class="cache">${JSON.stringify(this.items)}</div>
            </li>`
    }
}


class Logs {

    static run (query, cb) {
        console.log('module logs')
        const maxItems = parseInt(consts.packageInfos.maxitems)

        let searchtype = '-t '
        let params = ''
        if (query.search) {
            console.log('find by string')
            if (query.type == 0) {
                searchtype = '-u'
                console.log('unit find')
            }
            if (query.search.substring(0, 1) == '/') {
                searchtype = ''
                console.log('program find with path')
            }
            query.search = `"${query.search}"`
        } else {
            searchtype = ''
        }
        if (query.boot) {
            params += ' -b ' + query.boot
        } else {
            console.log('date:', query.date) // format 2015-12-31
            if (query.date) {
                console.log('find by date')
                params += ` --since "${query.date}"`
            }
        }
        if (query.level) {
            params += ' -p ' + query.level
        }

        let command = `journalctl ${params} ${searchtype} ${query.search} --no-pager -n${maxItems} -o json | sed 's/}$/},/g'`
        console.log(command)
        exec(command, {
            maxBuffer: maxItems * 40096,
            shell: '/bin/sh'
        }, (err, stdout, stderr) => {

            stdout = stdout.slice(0, -2) // text limit !!!
            console.log('return sizing: ', stdout.length)

            /*
            if (plus) {
                fs.writeFile(`${consts.tmp}stdout.json`, stdout, 'utf8', (err) => {
                    if (err) console.log('fs.writeFileSync error: ', err)
                })
            }
            */

            cb({
                'txt': '[' + stdout + ']',
                'error': stderr,
                'bash': command.slice(0, -25)
            })
        })
    }

    static toHtml (datas) {
        let items = []
        try {
            items = JSON.parse(datas.txt)
            console.log(items.length, ' JSON.parse Items .length')
        } catch (e) {
            console.error('JSON journal response Parsing error:', e)
        }

        // show messages
        console.log(items.length, ' items.length')
        datas.txt = ''
        items.forEach((item) => {
            delete item.__CURSOR
            item = new MessageItem(item, dico)
            datas.txt += item.render()
        })
        return datas.txt
    }
}

module.exports = Logs
