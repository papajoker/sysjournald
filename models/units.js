/*
*   get actives units
*/

const exec = require('child_process').exec
const execSync = require('child_process').execSync
// const consts = require('../app/consts.js')

class Units {

    static run (content, cb) {
        console.log('module boot')
        let response = {}
        response.displayManager = execSync("systemctl show display-manager.service | awk -F=  '/^Id/ {print $2}'", {
            encoding: 'utf8'
        })
        let bash = `systemctl list-units --state running -t service --no-pager --no-legend |` +
            `awk '{printf $1":";for(i=5;i<=NF;++i)printf $i" ";print ","}'`
        console.log(bash)
        exec(bash, (err, stdout, stderr) => {
            if (!err) {
                stdout = '{"' + stdout.replace(/:/g, '":"').replace(/,\n/g, "\",\"").slice(0, -2) + '}'
                try {
                    response.units = JSON.parse(stdout)
                    cb(response)
                    return true
                } catch (e) {
                    console.log('json ERROR', e, stdout)
                }
            } else {
                console.log('bash ERROR', bash, stderr)
            }
        })
        return false
    }

}

module.exports = Units
