const exec = require('child_process').exec

class Boot {

    static run (content, cb) {
        console.log('module boot')
        exec(`systemd-analyze plot`,
            (err, stdout, stderr) => {
                if (!err) {
                    cb(stdout)
                } else {
                    console.log('bash ERROR', stderr)
                }
            })
    }

    /*
    static run1 (content, cb) {
        // pas possible d'accès au disque'
        console.log('module boot')
        let src = `${__dirname}/../assets/img/plot.svg`
        exec(`systemd-analyze plot > ${src}`,
            (err, stdout, stderr) => {
                if (!err) {
                    cb(src)
                } else {
                    console.log('bash ERROR', stderr)
                }
            })
    }
    */
}

module.exports = Boot
