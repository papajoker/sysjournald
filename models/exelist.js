const exec = require('child_process').exec

class ExeList {

    static run (param, cb) {
        console.log('module exes')
        let response = {}
        exec(`journalctl -F _EXE | awk -F'/' '{print $NF}' | sort`, {
            encoding: 'utf8'},
        (err, stdout) => {
            if (!err) {
                response.exe = stdout
                exec(`journalctl -F UNIT --no-pager | grep -Ev "\\x2" | sort`, {
                    encoding: 'utf8'
                },
                (err, stdout) => {
                    if (!err) {
                        response.unit = stdout
                        cb(response)
                        return true
                    }
                }
                )
            }
        })
        return false
    }


    static toHtml (response) {
        response.unit = response.unit.replace(/(.*)\.service$/gm, '$1').replace(/\n/g, '<br />')
        response.exe = response.exe.replace(/\n/g, '<br />')
        return response
    }

}

module.exports = ExeList
