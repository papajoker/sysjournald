/*
*   get list boots in journald
*/
const exec = require('child_process').exec

class BootList {

    static run (content, cb) {
        console.log('module boot')
        let items, response = {}
        response.items = []
        exec(`journalctl --list-boots --no-pager | ` +
            `tail -n -40 | sort -gr | ` +
            `awk '{print $1","$3","$4","$5}'`, {
                encoding: 'utf8'},
                (err, stdout, stderr) => {
                    if (!err) {
                        let lines = stdout.split("\n")
                        lines.forEach((line) => {
                            items = line.split(',')
                            if (items.length == 4) response.items.push({
                                id: items[0],
                                day: items[1],
                                date: items[2].slice(5),
                                time: items[3]
                            })
                        })
                        cb(response)
                    }
                }
        )
    }

}

module.exports = BootList
